# Runners

Runners are the drivers which handle the results of the interface calculations.   
A basic and most frequently used runer is **optimizer** which reads the energy, gradiants (and optionaly Hessian) from the external molecular modelling program and drives the structure optimization. 

## Runners

* [Optimizer](Optimizer.md)
* MD (molecular dynamics)
* Transition state search
* ...

