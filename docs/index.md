# MonaLisa Documentation page

<img src="img/monalisa_logo.png"  alt="monalisa logo" width=250 />

This is the documentation page of the [MonaLisa program](https://www.chemie.hu-berlin.de/de/forschung/quantenchemie/monalisa "MonaLisa webpage"), a multilevel optimizer developed in the [Quantum Chemistry group](https://www.chemie.hu-berlin.de/de/forschung/quantenchemie "AK Sauer webpage") of Professor Joachim Sauer at [Humbold-Universität zu Berlin](https://www.hu-berlin.de/de "Main webpage of Huboldt-Universitaet zu Berlin").

MonaLisa is an optimizer for molecules and extended systems.   
Its key feature is a generalized subtractive QM:QM:... scheme, also called multi-level embedding. In this approach a system (molecule) can be divided into arbitrary number of sub-parts which can be treated using different quantum chemistry methods, implemented in external programs. MonaLisa drives the optimization of the supersystem, but it can also do many more.

MonaLisa consists of the **Runners** and **Interfaces**

## Runners:

MonaLisa provides:

* structure optimizations
* transition state search
* molecular dynamics module
* thermodynamic function calculations

## Interfaces:

MonaLisa use the eternal molecular modeling programs to obtain the information about the molecular properties of the investigated molecular system. The interaction between MonaLisa and external programs proceed via so called **External Interfaces** which currently can handle the following programs:

* Turbomole
* ORCA 
* VASP 
* CP2K
* gulp
* Crystal 
* Gaussian
* MOLPRO

MonaLisa can also process the molecular data obtained from several external (but also internal) interfaces via so called **Interal Interfaces** which currently are:

* 