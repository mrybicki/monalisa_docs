# Quick Introcduction to MonaLisa

 MonaLisa Workflow

Running the MonaLisa jobs requires setting up the system of the folders in which the single point calculations of the external programs (leaf interfaces) are performed. This should be done using the following command:

`/path-to-binary/monalisa --setup monalisa.inp`

Where monalisa.inp file (the name may be customized) contain definition of the job type and potential energy surface. For more details see job types and interfaces documentation. The simple example of the input file for the MonaLisa optimization using Orca interface (`monalisa.inp`):


```bash
# definition of the molecular structure
structure      monalisa.car

# definition of the runner type
run {
    optimize
} 

# options of the optimizer
optimizer {
    # convergence criteria
    en_change     1.0e-6 # maximal energy change
    max_grad      5.0e-3 # maximal gradient
    max_step      5.0e-3 # maximal length of the step
    rms_grad      1.0e-3 # rms of the gradient
    rms_step      1.0e-3 # rms of the step
} 

# definition of the PES (Interfaces)
toplevel {
    #which interface should be used
    interface orca{
        # orca interface options
        program = orca #path to the binary of the program
        keywords = pbe D2 def2-TZVP
        nprocs = 1
        charge = 0
        multiplicity = 1
}
```

After running the setup job, MonaLisa prepares the toplevel/ folder in which all input files needed for calculations are placed. In the more complicated calculations  additional subfolders may be created.

After the setup calculations the production MonaLisa run may be invoked by:

`/path-to-binary/monalisa monalisa.inp > monalisa.out`

The output data will be placed in the monalisa.out file. After each step of optimization the updated structure is placed in the monalisa_dbg.car file, whereas hessian is saved in monalisa.hessian file.