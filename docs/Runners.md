# Runners

Runners are the drivers which handle the results of the interface calculations.   
A basic and most frequently used runer is **optimizer** which reads the energy, gradiants (and optionaly Hessian) from the external molecular modelling program and drives the structure optimization. 


## Types

* [Optimizer](#optimizer)
* MD (molecular dynamics)
* Transition state search
* ...

1. a
2. b
3. c
4. d
5. e
6. f
7. g
8. h
9. i
10. j


## Optimizer
1. a
2. b
3. c
4. d
5. e
6. f
7. g
8. h
9. i
10. j
1. a
2. b
3. c
4. d
5. e
6. f
7. g
8. h
9. i
10. j

### Subtype
